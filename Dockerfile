FROM python:3.11

WORKDIR /bot

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

ENV GOOGLE_APPLICATION_CREDENTIALS="/bot/config/google-creds.json"

ENV PYTHONUNBUFFERED=1
ENV PYTHONIOENCODING=UTF-8

CMD python main.py
