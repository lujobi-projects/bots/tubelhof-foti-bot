
from config.config import ACCEPTED_IDS
from google.cloud import storage

def upload_blob(bucket_name, source_file_name, destination_blob_name):
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print(
        "File {} uploaded to {}.".format(
            source_file_name, destination_blob_name
        )
    )

def is_authenticated(message):
    if message.from_user.id not in ACCEPTED_IDS:
        print(message.from_user.id)
        message.reply_text("GTFO, azeig isch dusse")
        return False
    return True