
## Running

```bash
docker build . -t fotibot
docker run -it -v ${PWD}/config.py:/bot/config/config.py -v ${PWD}/google_creds.json:/bot/config/google_creds.json fotibot
```
