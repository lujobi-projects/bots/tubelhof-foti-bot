
from pyrogram import Client, filters
import uuid
import os
from config.config import *
from utils import *

app = Client("fotibot_tubelhof", API_ID, API_HASH, bot_token=BOT_TOKEN )

@app.on_message(filters.text & filters.private)
def echo(_, message):
    if not is_authenticated(message):
        return
    message.reply_text("Red ni mit mir alter, schick föteli oder las si")


@app.on_message(filters.private | filters.media_group | filters.photo | filters.video | filters.document )
def echo(_, message):
    if not is_authenticated(message):
        return
    if not message.media:
        message.reply_text("Weird shit")
        return

    # Keep track of the progress while downloading
    def progress(current, total):
        print(f"{current * 100 / total:.1f}%")

    try:
        place = app.download_media(message, progress=progress)
        upload_blob('tubelhof-fotis', place, place.split('/')[-1] if place else uuid.uuid4())
        os.remove(place)
            
        message.reply_text("Gut brudi")
    except Exception as e: 
        print(e)
        message.reply_text("Fähler brudi, mäud di bi @LuzianBieri")

@app.on_message()
def echo(_, message):
    if not is_authenticated(message):
        return
    message.reply_text("Dusse kläre, aber nur du u ig")


print("starting")
app.run()  # Automatically start() and idle()